#ifndef CARD_H
#define CARD_H
#include <string>
#include <vector>

class Card {
	public:
		int value;
		int suit;
		std::string value_name;
		std::string suit_name;

		std::vector<std::string> values = {
			"Ace", "Two", "Three", "Four", "Five",
			"Six", "Seven", "Eight", "Nine", "Ten",
			"Jack", "Queen", "King"
		};

		std::vector<std::string> suits = {
			"Hearts", "Diamonds", "Clubs", "Spades"
		};

		Card();
		Card(int v, int s);

		static int get_values_amount();
		static int get_suits_amount();
};

class Deck {
	public:
		std::vector<Card> cards;

		Deck();		
		void shuffle();
		std::string get_value_name(int i);
		std::string get_suit_name(int i);
};

#endif