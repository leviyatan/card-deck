#include "deck.h"

Card::Card(){
}

Card::Card(int v, int s){
	this->value = v;
	this->suit = s;
	this->value_name = values[v];
	this->suit_name = suits[s];
}

int Card::get_values_amount(){
	Card card;
	return card.values.size();
}

int Card::get_suits_amount(){
	Card card;
	return card.suits.size();
}

Deck::Deck(){
	// generates deck
	for (int i = 0; i < Card::get_suits_amount(); i++){
		for (int j = 0; j < Card::get_values_amount(); j++){
			Card card(j, i);
			cards.push_back(card);
		}
	}
}

void Deck::shuffle(){
	int random_card;
	std::vector<Card> shuffle;

	// shuffles deck
	while (!cards.empty()){
		random_card = rand()%cards.size();
		shuffle.push_back(cards[random_card]);
		cards.erase(cards.begin()+random_card);
	}

	cards = shuffle;
}

std::string Deck::get_value_name(int i){
	return cards[i].value_name;
}

std::string Deck::get_suit_name(int i){
	return cards[i].suit_name;
}