#include <iostream>
#include <time.h>
#include "deck.h"

int main(){
	std::cout << "Deck of cards\n" << std::endl;

	srand(time(NULL));

	Deck* deck = new Deck;
	deck->shuffle();

	int deck_size = Card::get_values_amount()*Card::get_suits_amount();

	// deals 5 cards
	for (int i = 0; i < deck_size; i++){
		std::cout << deck->get_value_name(i) << " of "
		<< deck->get_suit_name(i) << std::endl;
	}

	std::cout << "\n\n\n";

	delete deck;
	deck = new Deck;

	deck_size = Card::get_values_amount()*Card::get_suits_amount();

	// deals 5 cards
	for (int i = 0; i < deck_size; i++){
		std::cout << deck->get_value_name(i) << " of "
		<< deck->get_suit_name(i) << std::endl;
	}

	return 0;
}